const flatMap = require('unist-util-flatmap');

function flattenNestedLists() {
  return ast => {
    return flatMap(ast, node => {
      if (node.type !== 'list') return [node];
      const out = [];
      let latestContiguousList = Object.assign({}, node, {children: []});
      for (let listItem of node.children) {
        if (listItem.type !== 'listItem') throw new Error('malformed list');
        if (listItem.children.length < 1) throw new Error('malformed listItem');
        if (listItem.children[0].type === 'paragraph') {
          latestContiguousList.children.push(
            Object.assign({}, listItem, {children: [listItem.children[0]]}),
          );
        }
        for (let i = 1, n = listItem.children.length; i < n; i++) {
          const nestedList = listItem.children[i];
          if (nestedList && nestedList.type === 'list') {
            if (latestContiguousList.children.length > 0) {
              out.push(latestContiguousList);
              latestContiguousList = Object.assign({}, node, {children: []});
            }
            out.push(nestedList);
          }
        }
      }
      if (latestContiguousList.children.length > 0) {
        out.push(latestContiguousList);
      }
      return out;
    });
  };
}

module.exports = flattenNestedLists;
