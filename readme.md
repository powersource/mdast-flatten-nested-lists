# mdast-flatten-nested-lists

Avoids lists insides lists in a markdown AST ([MDAST](https://github.com/syntax-tree/mdast)) by bringing all nested lists to the same
root level.

## Installation

```sh
npm install mdast-flatten-nested-lists
```

## Usage

```js
// ...
var flattenNestedLists = require('mdast-flatten-nested-lists');

unified()
  .use(parse)
  .use(flattenNestedLists)
// ...
```

Markdown document:

```
# Title

- First
  - a
  - b
- Second
```

Input AST:

```
root[2] (1:1-8:1, 0-39)
├─ heading[1] (2:1-2:8, 1-8) [depth=1]
│  └─ text: "Title" (2:3-2:8, 3-8)
└─ list[2] (4:1-7:9, 10-38) [ordered=false][loose=false]
   ├─ listItem[2] (4:1-6:6, 10-29) [loose=false]
   │  ├─ paragraph[1] (4:3-4:8, 12-17)
   │  │  └─ text: "First" (4:3-4:8, 12-17)
   │  └─ list[2] (5:3-6:6, 20-29) [ordered=false][loose=false]
   │     ├─ listItem[1] (5:3-5:6, 20-23) [loose=false]
   │     │  └─ paragraph[1] (5:5-5:6, 22-23)
   │     │     └─ text: "a" (5:5-5:6, 22-23)
   │     └─ listItem[1] (6:3-6:6, 26-29) [loose=false]
   │        └─ paragraph[1] (6:5-6:6, 28-29)
   │           └─ text: "b" (6:5-6:6, 28-29)
   └─ listItem[1] (7:1-7:9, 30-38) [loose=false]
      └─ paragraph[1] (7:3-7:9, 32-38)
         └─ text: "Second" (7:3-7:9, 32-38)
```

Output AST:

```
root[4] (1:1-8:1, 0-39)
├─ heading[1] (2:1-2:8, 1-8) [depth=1]
│  └─ text: "Title" (2:3-2:8, 3-8)
├─ list[1] (4:1-7:9, 10-38) [ordered=false][loose=false]
│  └─ listItem[1] (4:1-6:6, 10-29) [loose=false]
│     └─ paragraph[1] (4:3-4:8, 12-17)
│        └─ text: "First" (4:3-4:8, 12-17)
├─ list[2] (5:3-6:6, 20-29) [ordered=false][loose=false]
│  ├─ listItem[1] (5:3-5:6, 20-23) [loose=false]
│  │  └─ paragraph[1] (5:5-5:6, 22-23)
│  │     └─ text: "a" (5:5-5:6, 22-23)
│  └─ listItem[1] (6:3-6:6, 26-29) [loose=false]
│     └─ paragraph[1] (6:5-6:6, 28-29)
│        └─ text: "b" (6:5-6:6, 28-29)
└─ list[1] (4:1-7:9, 10-38) [ordered=false][loose=false]
   └─ listItem[1] (7:1-7:9, 30-38) [loose=false]
      └─ paragraph[1] (7:3-7:9, 32-38)
         └─ text: "Second" (7:3-7:9, 32-38)
```

## License

[MIT](LICENSE)